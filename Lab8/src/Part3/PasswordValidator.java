/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dannylowpass
 */
public class PasswordValidator {
    
    protected static boolean checkLength(String password){
        return password.length() >=8;
    }
    
    protected static boolean checkSpecialCharacters(String password){
        Pattern p = Pattern.compile("[!@#$^&]");
        Matcher m = p.matcher(password);
        boolean b = m.find();
        return b;
    }
    
    protected static boolean checkUpperCase(String password){
        Pattern p = Pattern.compile("[A-Z]");
        Matcher m = p.matcher(password);
        boolean b = m.find();
        return b;
    }
    
    protected static boolean checkDigits(String password){
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(password);
        boolean b = m.find();
        return b;
    }
    
    protected static boolean validatePassword(String password){
        
       if (checkLength(password) && checkSpecialCharacters(password) &&
               checkUpperCase(password) && checkDigits(password) == true) {
        System.out.println("Password valid");
        return true;
       } else {
        System.out.println("Password is not valid");
        return false;
       }
      
    } 
}
