/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dannylowpass
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validatePassword length regular");
        String password = "abcdefghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length regular");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length regular");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword length regular");
        String password = "abcdefgh";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordSpecialCharactersRegular() {
        System.out.println("validatePassword special character regular");
        String password = "@abcdefghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the special characters requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
}
